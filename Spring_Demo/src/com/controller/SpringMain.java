package com.controller;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.to.Hello;

public class SpringMain {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("Demo.xml");
		Hello h = (Hello) context.getBean("h1");
		System.out.println(h.getMessage());
	}

}
