package com.gl;

import java.util.HashMap;
import java.util.Map;

public class FrequencyOfChar {
	public static void main(String[] args) {
		 String str = "abcrdacards";
	        
	       char[] arr= str.toCharArray();
	        Map<Character, Integer> map = new HashMap<Character, Integer>();
	        
	        for(char a : arr){
	            if(!map.containsKey(a)){
	                map.put(a, 1);
	            }
	            else{
	            
	  	               map.put(a, map.get(a)+1);
	            }
	        }
	        
	       for(Map.Entry<Character, Integer> entry : map.entrySet()) {
	    	 System.out.println( entry.getKey() + "   ==   "+ entry.getValue());
	       }
	        
	        
	}

}
