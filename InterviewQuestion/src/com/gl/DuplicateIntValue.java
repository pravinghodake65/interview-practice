package com.gl;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class DuplicateIntValue {
	public static void main(String[] args) {
	List<Integer> list = Arrays.asList(1,2,3,1,2,3);
	
	Set<Integer> set = new HashSet<Integer>();
	
	List<Integer> list1 =list.stream().filter(t -> set.add(t)).collect(Collectors.toList());
	list1.forEach(System.out::println);
	
	list.stream().map(t -> t*2).collect(Collectors.toList()).forEach(System.out::print);

	
	
	}

}
