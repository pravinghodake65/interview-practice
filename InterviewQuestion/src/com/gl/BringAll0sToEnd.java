package com.gl;

import java.util.Arrays;

public class BringAll0sToEnd {
	public static void main(String[] args) {
		int[] arr = {0,0,3,5,0,6, 1, 0, 2, 0 };
		System.out.println(Arrays.toString(arr));
		
		extracted(arr);
		System.out.println(Arrays.toString(arr));
	}

	private static void extracted(int[] arr) {
		int nonZeroIndex = 0;
		for(int index = 0; index< arr.length; index++) {
			if(arr[index] !=0) {
				int temp = arr[index];
				arr[index] = arr[nonZeroIndex];
				arr[nonZeroIndex]= temp;
				nonZeroIndex++;
			}
		}
	}
}
