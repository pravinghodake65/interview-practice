package com.gl.String;

public class EvenLetterWords {

	public static void main(String[] args) {
		String s = "The Sky is Blue and Vast";
		
		String [] arr=s.split(" ");

		for( String a: arr) {
			if(a.length()%2==0) {
				System.out.println(a);
			}
		}
	}

}
