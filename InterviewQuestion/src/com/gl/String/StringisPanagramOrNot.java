package com.gl.String;

public class StringisPanagramOrNot {

	public static void main(String[] args) {
		String string = "Fix problem quickly with galvanized jets";
		boolean res =check(string.toLowerCase());
		
		if(res == true) 
			System.out.println("String is panagram");
		else
			System.out.println("String is Not a panagram");
	}

	private static boolean check(String string) {
		if (string.length() < 26) {
			return false;
		} else {
			for (char i = 'a'; i < 'z'; i++) {
				if (string.indexOf(i) < 0) {
					return false;
				}
			}
			return true;
		}
	}

}
