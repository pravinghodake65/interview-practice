package com.gl.String;

public class ReverseAString {
	
	public static void main(String[] args) {
		String s = "hello";
		
		char [] a =s.toCharArray();
		
		
		System.out.println(a);
		
		int left = 0;
		int right = a.length-1;
		
		while(left < right) {
			char temp= a[left];
			a[left] = a[right];
			a[right] = temp;
			left++;
			right--;
		}
		
		System.out.println(a);
	}

}
